<%@include file="./init.jsp" %>

<%
Log _logger = LogFactoryUtil.getLog(this.getClass());
	GrabMyGrav gravDTO = new GrabMyGrav(user.getEmailAddress(), true);
	gravDTO.setDefaultImg(GravType.RETRO);
	gravDTO.setRating(GravRating.RESTRICTED);

	if(user != null) {
		if(user.getFullName() != null && user.getFullName().length() > 0) {
			_logger.info(user.getFullName());
			out.println("<div>"+user.getFullName()+"'s Profile</div>");
		}
		if(user.getGreeting() != null && user.getGreeting().length() > 0) {
			_logger.info(user.getGreeting());
			out.println("<div>"+user.getGreeting()+"</div>");
			
		}
		gravDTO.setImageSize(200);
		out.println("<div><img alt="+user.getEmailAddress()+" src="+GrabMyGravEngine.getFullGravURL(gravDTO)+" /></div>");
		gravDTO.setImageSize(100);
		out.println("<div><img alt="+user.getEmailAddress()+" src="+GrabMyGravEngine.getFullGravURL(gravDTO)+" /></div>");
		gravDTO.setImageSize(50);
		out.println("<div><img alt="+user.getEmailAddress()+" src="+GrabMyGravEngine.getFullGravURL(gravDTO)+" /></div>");
		gravDTO.setImageSize(32);
		out.println("<div><img alt="+user.getEmailAddress()+" src="+GrabMyGravEngine.getFullGravURL(gravDTO)+" /></div>");

		if(user.getJobTitle() != null && user.getJobTitle().length() > 0) {
			_logger.info(user.getJobTitle());
			out.println("<div>Job Title: "+user.getJobTitle()+"</div>");
		}
	}
%>

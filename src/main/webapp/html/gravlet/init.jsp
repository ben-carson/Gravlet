<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/security" prefix="liferay-security" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@page import="net.bencarson.grabmygrav.dto.GrabMyGrav"%>
<%@page import="net.bencarson.grabmygrav.engine.GrabMyGravEngine"%>
<%@page import="net.bencarson.grabmygrav.common.GravRating"%>
<%@page import="net.bencarson.grabmygrav.common.GravType"%>

<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>


<!-- Initialize portlet taglibs -->
<portlet:defineObjects/>

<!-- Initialize Liferay taglibs -->
<liferay-theme:defineObjects />